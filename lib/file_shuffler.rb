# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.


def file_shuffler
  puts 'Specify file to be shuffled: '
  file_path = gets.chomp
  file_name = file_path[0...file_path.index('.')]

  shuffled_file_lines = File.readlines(file_path).shuffle

  File.open("#{file_name}-shuffled.txt", 'w') do |file|
    shuffled_file_lines.each { |line| file.puts line.chomp }
  end
end

if __FILE__ == $PROGRAM_NAME
  file_shuffler
end
