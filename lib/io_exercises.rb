# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.


def guessing_game
  answer = rand(99) + 1
  num_guesses = 0
  guesses = []

  loop do
    puts 'Guess a number: '

    guesses.push(gets.chomp.to_i)
    num_guesses += 1
    print_guesses(guesses)

    if guesses.last > answer
      puts 'too high'
    elsif guesses.last < answer
      puts 'too low'
    else
      break
    end
  end

  puts "answer: #{answer} num_guesses: #{num_guesses}"
end

def print_guesses(guesses)
  str = ''
  guesses.each { |num| str << num.to_s << ' ' }
  puts str.strip
end

if __FILE__ == $PROGRAM_NAME
  guessing_game
end
